FROM node:12

RUN apt-get update && apt-get install -y libudev-dev libusb-1.0 libsecret-1-0

RUN git clone https://github.com/celo-org/celo-monorepo.git /celo

RUN cd /celo && yarn && yarn build --ignore docs

EXPOSE 8545

CMD cd /celo/packages/sdk/contractkit && yarn test:reset && yarn test:livechain